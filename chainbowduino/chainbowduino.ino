/* -*- c++ -*-

   This is firmware for rainbowduinos that are chained together.

*/

#include "comm.h"
#include "Rainbow.h"
#include <Wire.h>
#include <EEPROM.h>


#include "deutsch.h"
#include "RTClib.h"
/*TODO
 * Import RTC lib
 * 
 */

// Pin definitions
#define PIN_BUTTON PD7//TODO: add button and change pin here

Comm comm = Comm();
Rainbow rainbow = Rainbow();

RTC_DS1307 rtc;
unsigned long disp_sec;
unsigned long disp_min;
unsigned long disp_hrs;

volatile boolean updatenow = false;

boolean buttonState = LOW;
unsigned long buttonMillis = 0;
boolean buttonHandled = true;

boolean blink_enable = true;
boolean blinknow = false;

enum ClockMode {
  NORMAL,
  SET_MIN,
  SET_HRS,
  END,
};
ClockMode clockmode = NORMAL;

// color is packed into a 2byte int: 0x0bgr
unsigned int get_int()
{
    byte b1 = comm.read();
    byte b2 = comm.read();
    return (b1 << 8) | b2;
}

// read and unpack three bytes assuming [0xrg, 0xbR, 0xGB] 
// and repack them into two ints like [0x0bgr, 0x0BGR]
void get_two(unsigned int *two)
{
    byte b1 = comm.read();
    byte b2 = comm.read();
    byte b3 = comm.read();
    
    byte r = 0xf&(b1 >> 4);
    byte g = 0xf&(b1);
    byte b = 0xf&(b2 >> 4);
    byte R = 0xf&(b2);
    byte G = 0xf&(b3 >> 4);
    byte B = 0xf&(b3);

    two[0] = (b<<8) | (g<<4) | r;
    two[1] = (B<<8) | (G<<4) | R;
}

void get_eight(unsigned int *eight)
{
    for (int count = 0; count < 8; ++count) {
        eight[count] = 0;
    }
    for (int count = 0; count < 4; ++count) {
        get_two(eight+count*2);
    }
}



bool handle_packet(int nbytes)
{
    unsigned char cmd = comm.read();
    
    unsigned int color;
    unsigned int eight[8];
    unsigned int matrix[8][8];

    unsigned char row, col, pixel, ascii;

    switch (cmd) {

    case 'S':                   // Draw serial number
        rainbow.closeAll();
        ascii = (unsigned char)(comm.addr() + '0');
        rainbow.dispChar(ascii, WHITE, 0);
        break;

    case 'D':                   // Darken (clear) all LEDs
        rainbow.closeAll();
        break;

    case 'L':                    // Light all LEDs given color
        color = get_int();
        rainbow.lightAll(color);
        break;

    case 'P':                   // set one pixel to a color
        // format: 0xCR (4 bits for column and 4 bits for row)
        pixel = comm.read();
        col = pixel >> 4;
        row = pixel & 0x0F;

        // format: 0x0bgr
        color = get_int();
        rainbow.lightOneDot(row,col,color,OTHERS_ON);
        break;

    case 'R':                   // Set one row
        // format: 0x0R (row number) 0xrg 0xbR 0xGB * 4
        row = comm.read();
        get_eight(eight);
        rainbow.lightOneLine(row,eight,OTHERS_ON);
        break;

    case 'C':                   // Set one column
        // format: 0x0C (column number) 0x0bgr * 8 (8 ints of colors)
        col = comm.read();
        get_eight(eight);
        rainbow.lightOneColumn(col,eight,OTHERS_ON);
        break;

    case 'M':                   // Set whole matrix
        // row1's 8 columns first, row8's last
        for (int irow = 0; irow < 8; ++irow) {
            get_eight(eight);
            for (int icol = 0; icol < 8; ++icol) {
                matrix[irow][icol] = eight[icol];
            }
        }
        rainbow.lightAll(matrix);
        break;

    case 'A':                   // Set an ASCII letter
        // format: Letter(byte), color(int), offest(byte)
        ascii = comm.read();
        color = get_int();
        col = comm.read();
        rainbow.dispChar(ascii, color, col);
        break;

    default:
        return false;
        break;
    }
    return true;
}

void handle_i2c(int num)
{
    // byte addr = comm.read();
    // byte count = comm.read();

    unsigned char ascii = 0;

    /*
    rainbow.lightAll(WHITE);
    rainbow.closeAll();

    ascii = (unsigned char)(count + '0');
    rainbow.dispChar(ascii, WHITE, 0);
    delay(500);

    rainbow.lightAll(WHITE);
    rainbow.closeAll();
    ascii = (unsigned char)(addr + '0');
    rainbow.dispChar(ascii, WHITE, 0);
    delay(500);
    */

    /*
    if (addr != comm.addr() || count != num) {
        comm.wire_drain();
        return;
    }
    */

    handle_packet(num);

    /*
    delay(500);
    rainbow.lightAll(WHITE);
    rainbow.closeAll();
    ascii = (unsigned char)(addr + '0');
    rainbow.dispChar(ascii, WHITE, 0);
    */
}
void check_Button()
{
  if(digitalRead(PIN_BUTTON) != buttonState) {
    buttonState = digitalRead(PIN_BUTTON);
    if(buttonState == LOW) { // button was pressed
      buttonMillis = millis();
      buttonHandled = false;
    }
    else { // button was released
      buttonHandled = true;
      unsigned long buttonDelay = millis() - buttonMillis;
      if(buttonDelay > 100) { // debounce
        if(buttonDelay < 1000) { // simple press
          updatenow = true;
          switch(clockmode) {
          case NORMAL:
            blink_enable = !blink_enable;
            blinknow = true;
            TCNT1 = 0;
            break;
          case SET_MIN:
            rtc.adjust(rtc.now().unixtime() + 1*60);
            blinknow = true;
            TCNT1 = 0;
            break;
          case SET_HRS:
            rtc.adjust(rtc.now().unixtime() + 1*60*60);
            blinknow = true;
            TCNT1 = 0;
            break;
          }
        }
      }
    }
  }
  else {
    if(buttonState == LOW && !buttonHandled) { // button is being pressed
      unsigned long buttonDelay = millis() - buttonMillis;
      if(buttonDelay > 2000) {
        blinknow = false;
        TCNT1 = 0;
        clockmode = (ClockMode)((int)clockmode + 1);
        if(clockmode == END)
          clockmode = NORMAL;
        buttonHandled = true;
        updatenow = true;
      }
    }
  }
}


void updateTime() {
  // Adjust 2.5 minutes = 150 seconds forward
  // So at 12:03 it already reads "five past 12"
  DateTime now = rtc.now().unixtime() + 150;

  disp_sec = now.second();
  disp_min = now.minute();
  disp_hrs = now.hour();

  disp_hrs -= 1; //workaround switch hour
 // disp_min /= 5;

  if(disp_min >= min_offset)
    ++disp_hrs %= 12;
  else
    disp_hrs   %= 12;
}


void setup ()
{
    
    int addr = 0;//EEPROM.read(0); //0 to trigger I2C master

    comm.init(addr); //also enables serial communication
    //comm.set_handler(handle_packet);
    //Wire.onReceive(handle_i2c);

    Serial.print("SETUP AND AJUST TIME TO: ");
    Serial.print(DateTime(__DATE__, __TIME__).hour());   
    Serial.print(":"); 
    Serial.print(DateTime(__DATE__, __TIME__).minute());
    rtc.begin();
    rtc.adjust(DateTime(__DATE__, __TIME__));


    rainbow.init();
/*    rainbow.lightAll(WHITE);
    rainbow.closeAll();

    unsigned char ascii = (unsigned char)(comm.addr() + '0');
    rainbow.dispChar('W', WHITE, 0);
    rainbow.dispColor(AQUA);*/

}

void loop ()
{
    static unsigned char ascii=33;
//  comm.process();

    delay(5000);
    updateTime();
    Serial.print(disp_hrs);
    Serial.print(":");
    Serial.println(disp_min);
    rainbow.dispTime(disp_hrs, disp_min, AQUA);
  
    
    /*
     * delay(5000);
    rainbow.dispTime(8,35,WHITE);
    delay(5000);
    rainbow.dispTime(1,15,WHITE);
    delay(5000);
    rainbow.dispTime(11,40,WHITE);*/
    /*
    rainbow.dispChar(ascii, all_colors[random(7)], 0);
    ascii = ascii +1;
    if (ascii>120)
    {
      ascii =33;
    }
    */
    
}

//= //Timer1 interuption service routine=========================================
ISR(TIMER1_OVF_vect)         
{
    //sweep 8 lines to make led matrix looks stable
    static unsigned char line=0,level=0;

    flash_line(line,level);

    line++;
    if (line>7) {
        line=0;
        level++;
        if(level>15) {
            level=0;
        }
    }  
 
}
